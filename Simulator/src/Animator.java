import Objects.Bin;
import Objects.Item;
import Objects.ThreeD.Bin3D;
import Objects.ThreeD.Item3D;
import Objects.TwoD.Bin2D;
import Objects.TwoD.Corner2D;
import Objects.TwoD.Item2D;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

public class Animator  extends JPanel implements ActionListener {

    private static final int FRAME_HEIGHT = 720;
    private static final int FRAME_WIDTH = 1000;

    private ArrayList<Item> packages;

    private int index;

    private final int frameSpeed = 200;

    private double scale = 4;

    double binWidth;

    private Bin bin;

    Timer timer = new Timer(frameSpeed, this);

    double maxHeight;

    public Animator(String filePath){
        packages = new ArrayList<>();
        index = 0;
        maxHeight = 0;
        boolean flag = false;
        try {
            BufferedReader br = new BufferedReader(new FileReader(filePath));

            br.lines().forEach(s -> {
                String[] str = s.split(" ");
                if (str.length == 5){
                    // 2d
                    Item2D i = new Item2D((int)Double.parseDouble(str[0]), Double.parseDouble(str[1]), Double.parseDouble(str[2]));
                    i.placeItem(new Corner2D(Double.parseDouble(str[3]), Double.parseDouble(str[4])));
                    if (i.getHeight() + i.getY() > maxHeight){
                        maxHeight = i.getHeight() + i.getY();
                    }
                    packages.add(i);
                }else if (str.length == 7){
                    // 3d
                }else if (str.length == 1){
                    // strip width
                    binWidth = Double.parseDouble(str[0]);
                }

            });

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        if (packages.get(0) instanceof Item2D){
            bin = new Bin2D(100, 100);
        }else if (packages.get(0) instanceof Item3D){
            bin = new Bin3D(100, 100, 100);
        }

//        System.out.println(binWidth);
        scale = FRAME_HEIGHT/(maxHeight+(maxHeight*0.1));

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        repaint();
    }


    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
//        bin.drawBin(g, scale);
        g.setColor(Color.GREEN);
        g.drawLine((int)(binWidth*scale) , 0, (int)(binWidth*scale), FRAME_HEIGHT);
        for (int i = 0; i <= index;  i++) {
            packages.get(i).drawItem(g, scale);
        }
        if (!(index == packages.size()-1)) {
            index++;
        }
        timer.start();
    }

    public static void main(String[] args) {

        SwingUtilities.invokeLater(() -> {
            var panel = new Animator(args[0]);
            panel.setBackground(Color.WHITE);
            var frame = new JFrame("Animator");
            frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.getContentPane().add(panel, BorderLayout.CENTER);
            frame.setVisible(true);
            frame.setResizable(false);
        });
    }

}
