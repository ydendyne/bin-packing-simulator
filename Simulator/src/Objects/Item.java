package Objects;

import java.awt.*;

public interface Item {
    public void drawItem(Graphics g, double scale);

    }
