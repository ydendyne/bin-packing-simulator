package Objects.TwoD;

import java.awt.*;
import java.util.ArrayList;

public class Corner2D {

    private double x;
    private double y;

    private Item2D item2D;

    private ArrayList<Item2D> possibleItem2DS;

    public Corner2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void draw(Graphics g, double scale){
        g.setColor(Color.YELLOW);
        g.fillOval((int)(this.x*scale - scale), (int)(this.y*scale - scale), (int)(2 * scale), (int)(2 * scale));
    }

    @Override
    public String toString() {
        return "Corner2D{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
