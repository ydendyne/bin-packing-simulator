package Objects.TwoD;

import Objects.Item;

import java.awt.*;
import java.util.ArrayList;

public class Item2D implements Item {

    private int id;

    private double width;
    private double height;

    private double x;
    private double y;
    private boolean isPlaced = false;

    private double area;

    public Item2D(int id, double width, double height) {
        this.id = id;

        this.width = width;
        this.height = height;

        this.area = width * height;
    }

    public ArrayList<Corner2D> placeItem(Corner2D corner) {
        isPlaced = true;
        this.x = corner.getX();
        this.y = corner.getY();
        ArrayList<Corner2D> newCorners = new ArrayList<>();
        //new corner on the x plane
        newCorners.add(new Corner2D(corner.getX() + this.width, corner.getY()));
        //new corner on the y plane
        newCorners.add(new Corner2D(corner.getX() , corner.getY() +  this.height));
        return newCorners;
    }

    public ArrayList<Corner2D> placeItemLevel(Corner2D corner) {
        isPlaced = true;
        this.x = corner.getX();
        this.y = corner.getY();
        ArrayList<Corner2D> newCorners = new ArrayList<>();
        //new corner on the x plane
        newCorners.add(new Corner2D(corner.getX() + this.width, corner.getY()));
        //new corner on the y plane
//        newCorners.add(new Corner2D(corner.getX() , corner.getY() +  this.height));
        return newCorners;
    }

    public double getArea(){
        return this.area;
    }

    public boolean isPlaced() {
        return isPlaced;
    }

    @Override
    public void drawItem(Graphics g, double scale){
        if (!isPlaced)return;

        g.setColor(Color.BLACK);
        g.fillRect((int)(x*scale),(int)(y*scale),(int)(width*scale),(int)(height*scale));
        g.setColor(Color.RED);
        g.drawRect((int)(x*scale),(int)(y*scale),(int)(width*scale),(int)(height*scale));
        g.drawString(""+getId(), (int)(x*scale)+2,(int)(y*scale)+15);

    }

    public Point2D[] getExtremePoints(){
        Point2D[] xp = new Point2D[4];
        xp[0] = new Point2D(this.x,this.y);
        xp[1] = new Point2D(this.x + this.width,this.y);
        xp[2] = new Point2D(this.x,this.y + this.height);
        xp[3] = new Point2D(this.x + this.width,this.y + this.height);
        return xp;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return (isPlaced)?

                id +
                " " + width +
                " " + height +
                " " + x +
                " " + y

                :

                "id=" + id +
                ", width=" + width +
                ", height=" + height;
    }
}
