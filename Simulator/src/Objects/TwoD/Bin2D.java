package Objects.TwoD;

import Objects.Bin;
import Objects.ThreeD.Item3D;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Bin2D implements Bin {

    private List<Item2D> item2DS;
    private ArrayList<Corner2D> corners;
    private double utilization;
    private double width;
    private double height;

    private double area;
    private double placedArea;

    private double x;
    private double y;

    public Bin2D(double width, double height){
        this.width = width;
        this.height = height;
        this.area = width * height;
        this.x = 10;
        this.y = 10;

        this.corners = new ArrayList<>();
        this.item2DS = new ArrayList<>();
        this.utilization = 0;
    }

    /**
     * Used for level algorithms, levels treated like bins
     *
     */
    public void placeInLevel2D(Item2D i, Corner2D c){
        corners.remove(c);
        this.corners.addAll(i.placeItemLevel(c));
        this.item2DS.add(i);
        updateUtilization(i);

    }

    public void addItem(Item2D i){
        this.item2DS.add(i);
        updateUtilization(i);
    }

    private void updateUtilization(Item2D i){
        placedArea += i.getArea();
        utilization = placedArea / area;
    }

    @Override
    public void drawBin(Graphics g, double scale){
        g.setColor(Color.GREEN);
        g.drawRect((int)(x*scale), (int)(y*scale), (int)(width*scale), (int)(height*scale));
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public List<Item2D> getItems2D() {
        return item2DS;
    }

    public ArrayList<Corner2D> getCorners() {
        return corners;
    }
}
