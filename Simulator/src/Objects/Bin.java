package Objects;

import java.awt.*;

public interface Bin {
    public void drawBin(Graphics g, double scale);
}
