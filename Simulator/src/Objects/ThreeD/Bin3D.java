package Objects.ThreeD;

import Objects.Bin;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Bin3D implements Bin {

    private List<Item3D> item3DS;
    private double utilization;
    private double width;
    private double height;
    private double depth;

    private double volume;
    private double placedVolume;

    public Bin3D(double width, double height, double depth){
        this.width = width;
        this.height = height;
        this.depth = depth;
        this.volume = width * height * depth;

        this.item3DS = new ArrayList<>();
        this.utilization = 0;
    }

    public void addItem(Item3D i){
         this.item3DS.add(i);
         updateUtilization(i);
    }

    private void updateUtilization(Item3D i){
        placedVolume += i.getVolume();
        utilization = placedVolume/volume;
    }

    @Override
    public void drawBin(Graphics g, double scale) {

    }
}
