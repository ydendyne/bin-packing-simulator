package Objects.ThreeD;

public class Item3D {

    private double width;
    private double height;
    private double depth;

    private double x;
    private double y;
    private double z;

    private double volume;

    public Item3D(double width, double height, double depth) {
        this.width = width;
        this.height = height;
        this.depth = depth;

        this.volume = width * height * depth;
    }

    public double getVolume(){
        return this.volume;
    }

}
