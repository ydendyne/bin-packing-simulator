import DataGen.Parser;
import DataGen.Parser2D;
import Objects.Bin;
import Objects.Item;
import Objects.TwoD.Bin2D;
import Objects.TwoD.Corner2D;
import Objects.TwoD.Item2D;
import Objects.TwoD.Point2D;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class BinPacking2D{

    public BinPacking2D(ArrayList<Bin> bins, ArrayList<Item> items, String filePath) {
        // converting from bin to 2D bin
        ArrayList<Bin2D> bin2D = new ArrayList<>();

        for (Bin bin : bins) {
            bin2D.add((Bin2D) bin);
        }

        // converting from item to 2D item
        ArrayList<Item2D> item2D = new ArrayList<>();
        for (Item item : items) {
            item2D.add((Item2D)item);
        }

        firstFit(bin2D, item2D, filePath);

    }

//    private void sortItems(){
//        items.sort((a,b)->{
//            return Double.compare(b.getArea(), a.getArea());
//        });
//        System.out.println(items);
//    }

    int i = 0;
    int x = 0;
    int y = 0;

    /**
     * Computes a packing for the specified items, saves the package sequence and positions in
     * a data file specified
     *
     * @param bins a list of bins (initialised with one bin)
     * @param items a list of items to be packed
     * @param filePath the file to save the packing into
     */
    public void firstFit(ArrayList<Bin2D> bins, ArrayList<Item2D> items, String filePath) {

        Collections.shuffle(items);
        ArrayList<Corner2D> corners = new ArrayList<>();
        ArrayList<Item2D> placed = new ArrayList<>();

        //get (0,0) of the initial bin
        corners.add(new Corner2D(bins.get(0).getX(), bins.get(0).getY()));
        System.out.println(corners.get(0));

        Item toPlace = null;
        Corner2D toRemove = null;
        ArrayList<Corner2D> cornersGenerated = new ArrayList<>();

        long timeStart = System.currentTimeMillis();

        do {
            toPlace = null;
            toRemove = null;

            for (Item2D item : items) {

                for (Corner2D corner : corners) {

                    if (isFeasible(corner, item, bins.get(0), placed)) {
                        cornersGenerated = item.placeItem(corner);
                        placed.add(item);

                        toPlace = item;
                        toRemove = corner;

                        bins.get(0).addItem(item);
                        break;
                    }
                }

                if (toPlace != null) {
                    break;
                }
            }

            if (toPlace != null) {
                items.remove(toPlace);
                corners.remove(toRemove);
            }

            corners.addAll(cornersGenerated);

        } while (toPlace != null);

        long totalTime = System.currentTimeMillis() - timeStart;
        System.out.println(totalTime);

        // write packed items to an array list
        try {

            BufferedWriter writer = new BufferedWriter(new FileWriter(filePath));

            for (Item2D item2D : placed) {
                writer.write(item2D.toString()+"\n");
            }

//            writer.write(placed.toString());
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private boolean isIntersection(Corner2D c, Item2D toPlace, Item2D placed){

        Point2D[] xp = placed.getExtremePoints();

        // If one item is on left side of other
        if (c.getX() >= xp[3].getX() || xp[0].getX() >= c.getX() + toPlace.getWidth()) {
            return false;
        }

        // If one item is above other
        if (c.getY() >= xp[3].getY() || xp[0].getY() >= c.getY() + toPlace.getHeight()) {
            return false;
        }

        return true;

    }

    /**
     * Determines weather a placement of an item on a particular corner
     * is feasible or not (in terms of intersection with the bin)
     *
     * - may need to include intersection with other packages
     */
    private boolean isFeasible(Corner2D c, Item2D i, Bin2D b, ArrayList<Item2D> placed){
        if (c.getX() + i.getWidth() > b.getX() + b.getWidth()){
            return false;
        }else if (c.getY() + i.getHeight() > b.getY() + b.getHeight()){
            return false;
        }

        for (Item2D item2D : placed) {
            if (isIntersection(c, i, item2D)){
                return false;
            }
        }

        return true;
    }

    public static void main(String[] args) {
        Parser p = new Parser2D("/home/dynehayd/IdeaProjects/code-and-data/Simulator/src/Data/1DBPPdata.dat");
        new BinPacking2D(p.getBins(), p.getItems(), args[0]);
    }

}
