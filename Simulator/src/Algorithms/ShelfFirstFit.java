package Algorithms;

import Objects.TwoD.Bin2D;
import Objects.TwoD.Corner2D;
import Objects.TwoD.Item2D;

import java.util.ArrayList;

public class ShelfFirstFit extends AbstractAlgorithm {

    public ShelfFirstFit() {
        super();
    }

    @Override
    public double optimize2D(double binWidth, ArrayList<Item2D> items, String filePath) {
        //        Collections.shuffle(items);

//        items.sort((a,b)->{
//            return Double.compare(b.getArea(), a.getArea());
//        });

        // initialize the first level with the height of the first package
        ArrayList<Bin2D> levels = new ArrayList<>();
        levels.add(new Bin2D(binWidth, items.get(0).getHeight()*1.1));
        levels.get(0).placeInLevel2D(items.get(0), new Corner2D(0,0));

        // start the timer for the algorithm
        long timeStart = System.currentTimeMillis();

        boolean placementFlag;
        for (Item2D item : items) {
            if (item.isPlaced()){
                continue;
            }
            placementFlag = false;
            // iterate over levels
            for (Bin2D level : levels) {

                // iterate over corners within a level
                for (Corner2D corner : level.getCorners()) {

                    // place an item in the current level if it fits
                    if (item.getHeight() <= level.getHeight() && corner.getX() + item.getWidth() <= level.getWidth()){
                        level.placeInLevel2D(item, corner);
                        placementFlag = true;
                        break;
                    }

                }

                // no need to iterate over any more levels
                if (placementFlag){
                    break;
                }

            }

            // didn't fit in any level so place it in a new level
            if (!placementFlag){
                Bin2D nextLevel = new Bin2D(binWidth, item.getHeight()*1.1);
                nextLevel.placeInLevel2D(item, new Corner2D(0, 0));
                levels.add(nextLevel);
            }
        }

        // time testing
        long totalTime = System.currentTimeMillis() - timeStart;
        System.out.println(totalTime);

        // stack each level
        double currentHeight = levels.get(0).getHeight();
        if (levels.size()>1) {
            for (int i = 1; i < levels.size(); i++) {
                for (Item2D item2D : levels.get(i).getItems2D()) {
                    item2D.setY(item2D.getY() + currentHeight);
                }
                currentHeight += levels.get(i).getHeight();
            }
        }

        // determine the height of the highest item
        calculateFitness2D(items);

        // write packed items to an array list
        save2D(binWidth, items, filePath);

        return fitness;
    }


}
