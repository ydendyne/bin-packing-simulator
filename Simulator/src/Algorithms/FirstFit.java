package Algorithms;

import Objects.Item;
import Objects.TwoD.Corner2D;
import Objects.TwoD.Item2D;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class FirstFit extends AbstractAlgorithm{

    public FirstFit() {
        super();
    }

    @Override
    public double optimize2D(double binWidth, ArrayList<Item2D> items, String filePath) {

//        Collections.shuffle(items);
        ArrayList<Corner2D> corners = new ArrayList<>();
        ArrayList<Item2D> placed = new ArrayList<>();

        //get (0,0) of the strip
        corners.add(new Corner2D(0, 0));

        Item toPlace = null;
        Corner2D toRemove = null;
        ArrayList<Corner2D> cornersGenerated = new ArrayList<>();

        long timeStart = System.currentTimeMillis();

        do {
            toPlace = null;
            toRemove = null;

            for (Item2D item : items) {

                for (Corner2D corner : corners) {

                    if (isFeasible2D(corner, item, binWidth, placed)) {
                        cornersGenerated = item.placeItem(corner);
                        placed.add(item);

                        toPlace = item;
                        toRemove = corner;

                        break;
                    }
                }

                if (toPlace != null) {
                    break;
                }
            }

            if (toPlace != null) {
                items.remove(toPlace);
                corners.remove(toRemove);
            }

            corners.addAll(cornersGenerated);

        } while (!items.isEmpty());

        long totalTime = System.currentTimeMillis() - timeStart;
        System.out.println(totalTime);

        calculateFitness2D(placed);
        save2D(binWidth, placed, filePath);

        return fitness;
    }

}
