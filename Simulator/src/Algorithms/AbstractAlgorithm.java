package Algorithms;

import Objects.TwoD.Corner2D;
import Objects.TwoD.Item2D;
import Objects.TwoD.Point2D;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public abstract class AbstractAlgorithm {

    public double fitness;

    public AbstractAlgorithm() {
        // initialize fitness to -1
        this.fitness = -1;
    }

    public abstract double optimize2D(double binWidth, ArrayList<Item2D> items, String filePath);

    /**
     *
     * @param c corner specified
     * @param toPlace item that is trying to fit into the corner c
     * @param placed a placed item
     * @return if to place intersects with placed if to place is placed at corner c
     */
    public boolean isIntersection2D(Corner2D c, Item2D toPlace, Item2D placed){

        Point2D[] xp = placed.getExtremePoints();

        // If one item is on left side of other
        if (c.getX() >= xp[3].getX() || xp[0].getX() >= c.getX() + toPlace.getWidth()) {
            return false;
        }

        // If one item is above other
        if (c.getY() >= xp[3].getY() || xp[0].getY() >= c.getY() + toPlace.getHeight()) {
            return false;
        }

        return true;

    }

    /**
     *
     * Determines weather a placement of an item on a particular corner
     * is feasible or not (in terms of intersection with the strip)
     *
     * - may need to include intersection with other packages
     */
    public boolean isFeasible2D(Corner2D c, Item2D i, double binWidth, ArrayList<Item2D> placed){
        //check if it can fit into the strip
        if (c.getX() + i.getWidth() > binWidth){
            return false;
        }

        //check that it doesn't overlap with another package
        for (Item2D item2D : placed) {
            if (isIntersection2D(c, i, item2D)){
                return false;
            }
        }

        return true;
    }

    /**
     *  Finds the item at the top of the strip, this height is the fitness
     *  of the algorithm so the fitness is updated
     *
     * @param placed all items that are in the strip
     */
    public void calculateFitness2D(ArrayList<Item2D> placed ){
        double max = 0;
        for (Item2D i : placed) {
            if (i.getY()+i.getHeight() > max){
                max = i.getY()+i.getHeight();
            }
        }
        System.out.println("Max Height (Fitness) = " + max);
        this.fitness = max;
    }

    /**
     * Saves item and coordinate information of every item in the strip
     * to a specified file
     *
     * @param placed items in the strip
     * @param filePath file to save to
     */
    public void save2D(double binWidth ,ArrayList<Item2D> placed , String filePath){
        try {

            BufferedWriter writer = new BufferedWriter(new FileWriter(filePath));
            writer.write(binWidth+"\n");

            for (Item2D item2D : placed) {
                writer.write(item2D.toString()+"\n");
            }

            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public double getFitness() {
        return fitness;
    }

//    private void sortItems(){
//        items.sort((a,b)->{
//            return Double.compare(b.getArea(), a.getArea());
//        });
//        System.out.println(items);
//    }

}
