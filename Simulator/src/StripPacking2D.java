import Algorithms.FirstFit;
import Algorithms.LevelFirstFit;
import Algorithms.ShelfFirstFit;
import DataGen.Parser;
import DataGen.Parser2D;
import Objects.Bin;
import Objects.Item;
import Objects.TwoD.Bin2D;
import Objects.TwoD.Corner2D;
import Objects.TwoD.Item2D;
import Objects.TwoD.Point2D;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class StripPacking2D {

    private double binWidth;

    public StripPacking2D(ArrayList<Bin> bins, ArrayList<Item> items, String filePath) {
        this.binWidth = ((Bin2D)bins.get(0)).getWidth();

        // converting from item to 2D item
        ArrayList<Item2D> item2D = new ArrayList<>();
        for (Item item : items) {
            item2D.add((Item2D)item);
        }
        optimize( binWidth, item2D, filePath);
    }

    public double optimize(double binWidth, ArrayList<Item2D> items, String filePath){

        double ret;
//        ret = new FirstFit().optimize2D( binWidth, items, filePath);
//        ret = new LevelFirstFit().optimize2D( binWidth, items, filePath);
        ret = new ShelfFirstFit().optimize2D( binWidth, items, filePath);
        return ret;
    }


    public static void main(String[] args) {
        Parser p = new Parser2D("/home/dynehayd/IdeaProjects/code-and-data/Simulator/src/Data/1DBPPdata.dat");
        new StripPacking2D(p.getBins(), p.getItems(), args[0]);
    }


}
