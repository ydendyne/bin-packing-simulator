package DataGen;

import Objects.TwoD.Bin2D;
import Objects.TwoD.Item2D;

public class Parser2D extends Parser{

    public Parser2D(String fileName){
        super(fileName);
    }

    @Override
    protected void parseBin(String bin){
        String[] str = bin.split(" ");
        bins.add(new Bin2D(Double.parseDouble(str[1]),Double.parseDouble(str[2])));
    }

    @Override
    protected void parseItem(String item) {
        String[] str = item.split(" ");
        items.add(new Item2D(Integer.parseInt(str[0]), Double.parseDouble(str[1]), Double.parseDouble(str[2])));
    }


}
