package DataGen;

import Objects.Bin;
import Objects.Item;
import Objects.TwoD.Bin2D;
import Objects.TwoD.Item2D;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public abstract class Parser {

    ArrayList<Item> items;
    ArrayList<Bin> bins;

    public Parser(String fileName){
        items = new ArrayList<>();
        bins = new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            br.lines().skip(1).forEach(s -> {
                if (s.contains("bin")){
                    parseBin(s);
                }else if (!s.contains("EOF")){
                    parseItem(s);
                }
            });

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    protected abstract void parseBin(String bin);

    protected abstract void parseItem(String item);

    public ArrayList<Bin> getBins() {
        return bins;
    }

    public ArrayList<Item> getItems() {
        return items;
    }

}
